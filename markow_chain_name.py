import random
from typing import List
import codecs
import sys
import argparse


def build_table(names: List[str], n):
    table = {}

    for name in names:

        for (i, _) in enumerate(name):
            j = i + n
            prefix = name[i:j]
            if j == len(name):
                if prefix not in table:
                    table[prefix] = []
                break

            suffix = name[j]
            if prefix in table:
                table[prefix].append(suffix)
            else:
                table[prefix] = [suffix]

    return table


def markov_chain(names, n, max_name_length, name_count):
    table = build_table(names, n)
    possible_prefixes = list(table.keys())
    generated_names = []

    while len(generated_names) < name_count:
        prefix = random.choice(possible_prefixes)
        while prefix[0].islower():
            prefix = random.choice(possible_prefixes)
        generated_name = prefix
        name_length = random.randint(4, max_name_length)

        while len(generated_name) < name_length:
            possible_suffixes = table[prefix]
            if not possible_suffixes:
                break
            suffix = random.choice(possible_suffixes)

            generated_name += suffix

            prefix = generated_name[-n:]

        generated_names.append(generated_name.capitalize())

    return generated_names


parser = argparse.ArgumentParser(
    description="Generate random names based on a Markov chain and a list of names in text form."
)
parser.add_argument(
    "name_file",
    metavar="name_file",
    type=str,
    help="File with names",
)

parser.add_argument(
    "-n",
    "--n",
    default=2,
    type=int,
    help="n is a parameter for the algorithm to determine prefix length, default: 2",
)

parser.add_argument(
    "-l",
    "--max-length",
    default=8,
    type=int,
    help="the maximal length of a name, default: 8",
)

parser.add_argument(
    "-a",
    "--amount",
    default=20,
    type=int,
    help="the amount of names to be generated, default: 20",
)

parser.add_argument(
    "-s",
    "--split",
    default=",",
    type=str,
    help="the character sequence that delimits the name in the name_file, default: ','",
)

args = parser.parse_args()

print(args.name_file)

with codecs.open(args.name_file, "r", "utf8") as f:
    for name in markov_chain(
        f.read().split(args.split), args.n, args.max_length, args.amount
    ):
        print(name)
