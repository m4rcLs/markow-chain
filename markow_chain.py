import random


def build_table(text, n):
    table = {}
    words = text.split(" ")

    for (i, _) in enumerate(words):
        j = i + n
        prefix = " ".join(words[i:j])
        if j == len(words):
            if prefix not in table:
                table[prefix] = []
            break

        suffix = words[j]
        if prefix in table:
            table[prefix].append(suffix)
        else:
            table[prefix] = [suffix]

    return table


def markov_chain(text, n, word_count):
    table = build_table(text, n)
    possible_prefixes = list(table.keys())

    prefix = random.choice(possible_prefixes)
    text = prefix
    while len(text.split(" ")) < word_count:
        possible_suffixes = table[prefix]
        if not possible_suffixes:
            break
        suffix = random.choice(possible_suffixes)

        text = " ".join([text, suffix])

        prefix = " ".join(text.split(" ")[-n:])
    return text.capitalize()


with open("alice_oz.txt", "r") as f:
    print(markov_chain(f.read(), 3, 300))
