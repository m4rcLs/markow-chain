import markow_chain


def test_markow_chain():
    text = "now he is gone she said he is gone for good"
    table = {
        "now he": ["is"],
        "he is": ["gone", "gone"],
        "is gone": ["she", "for"],
        "gone she": ["said"],
        "she said": ["he"],
        "said he": ["is"],
        "gone for": ["good"],
        "for good": [],
    }

    assert table == markow_chain.build_table(text, 2)
